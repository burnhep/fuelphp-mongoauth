<?php
/**
 * Mognodb Auth driver.
 */

 /**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.0
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2011 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * NOTICE:
 *
 * If you need to make modifications to the default configuration, copy
 * this file to your app/config folder, and make them in there.
 *
 * This will allow you to upgrade fuel without losing your custom config.
 */

return array(

	/**
	 * MongoDB connection group, leave default to use mongo array('default')
	 * to change default go to your app/congig/db.php
	 * or see http://docs.fuelphp.com/classes/mongo/introduction.html for more info
	 */
	'group' => 'default',

	/**
	 * MongoDB collection for users
	 */
	'collection' => 'users',

	/**
	 * MongoDB collection fields to use leave empty to get all
	 */
	'collection_fields' => array(
	//	'username',
	//	'email',
	//	'password',
	//	'login_hash'
	),

	/**
	 * $_POST key for login username
	 */
	'email_post_key' => 'email',

	/**
	 * $_POST key for login password
	 */
	'password_post_key' => 'password',

	/**
	 * This will allow you to use the group & acl driver for non-logged in users
	 */
	'guest_login' => false,

	/**
	 * Groups as id => array(name => <string>, roles => <array>)
	 */
	'groups' => array(
		 -1 => array('name' => 'Banned', 'roles' => array('banned')),
		 0   => array('name' => 'Guests', 'roles' => array()),
		 1   => array('name' => 'Users', 'roles' => array('user')),
		 50 => array('name' => 'Moderators', 'roles' => array('user', 'moderator')),
		 100  => array('name' => 'Administrators', 'roles' => array('user', 'moderator', 'admin')),
	),

	/**
	 * Salt for the login hash
	 */
	'login_hash_salt' => 'your_salt',


	/**
	 * Roles as name => array(location => rights)
	 */
	'roles' => array(
		/**
		 * Examples
		 * ---
		 *
		 * Regular example with role "user" given create & read rights on "comments":
		 *   'user'  => array('comments' => array('create', 'read')),
		 * And similar additional rights for moderators:
		 *   'moderator'  => array('comments' => array('update', 'delete')),
		 *
		 * Wildcard # role (auto assigned to all groups):
		 *   '#'  => array('website' => array('read'))
		 *
		 * Global disallow by assigning false to a role:
		 *   'banned' => false,
		 *
		 * Global allow by assigning true to a role (use with care!):
		 *   'super' => true,
		 */
	),

);
